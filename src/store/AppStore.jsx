export const createAppStore = (props) => {
    return {
      ships: [],
      usedShips: [],
      setShips: function (newShips) {
        this.ships = newShips;
      },
      factors: [],
      period: {
        start: "",
        end: ""
      }
    };
  };