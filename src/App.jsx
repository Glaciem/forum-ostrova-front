
import './App.css';
// import MainBlock from './components/main';
import { Theme, ThemePanel, Grid, Box, Container, Text, Button, Heading, Flex, Table } from '@radix-ui/themes';
import { GearIcon } from '@radix-ui/react-icons'
import ShipChoises from './components/ShipChoises';
import ListDays from './components/ListDays';
import MyDatePicker from './components/MyDatePicker';
import {axiosInstance} from './api/Api';
import { useAppStore } from './store/AppStoreProvider';
import ResultTable from './components/ResultTable';
import { useState } from 'react';
import ShiptTimeline from './components/ShipTimeLine';


function App() {

  const [results, setResults] = useState([]);

  const appStore = useAppStore();

  const sendFormInfo = () => {
    axiosInstance.post("/send_info", {
      period: {start: appStore.period.start, end: appStore.period.end},
      ships: appStore.usedShips,
      factors: appStore.factors.map((el) => {
        if (el.time === "+24") {
          el.time = "*";
        }
        return el
      })
    })
    .then(function (response) {
      setResults(response.data);
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    })
    .finally(function () {
    });
  }

  return (
    <div className="App">
      <Theme hasBackground={true} >
        <div style={{backgroundColor: "#374150", height: "45px", width: "100%", marginBottom: "15px"}}> 
          <Container size="4" style={{height: "100%"}} >
            <Flex direction={"row"} justify={"between"} align={"center"} style={{height: "45px"}} >
              {/* <Box> */}
                <Heading size="3"><font size="4" style={{color: "white", }}>Маршрут построен</font></Heading>
                {/* <img src={"./ship.png"} /> */}
              {/* </Box> */}
              
              <GearIcon size="3" color='white' style={{ height: 25, width: 25, cursor: "pointer" }} onClick={()=>(window.location = "http://127.0.0.1:8000/admin/app/")}/>
            </Flex>
          </Container>
        </div>
        <Container size="4">
          <Grid columns="3" gap="6">
            <Box >
            <Flex align={"center"} justify={"start"} >
              <ShipChoises/>
            </Flex>
            </Box>
            <Box >
            <Flex align={"center"} justify={"center"}>
              <ListDays />
              </Flex>
            </Box>

            <Box>
              <Flex direction={"column"} align={"end"} justify={"space-between"} style={{height: "100%"}}>
                <MyDatePicker/>
                <Button size="3" onClick={sendFormInfo}>Обработать</Button>
              </Flex>
            </Box>

            
          </Grid>
          <Box style={{paddingTop: "15px"}}>
            <ResultTable results={results}/>
          </Box>

          <Box style={{paddingTop: "15px"}}>
              <ShiptTimeline results={results}/>
          </Box>
        </Container>
        {/* <ThemePanel /> */}
      </Theme>
    </div>
  );
}

export default App;
