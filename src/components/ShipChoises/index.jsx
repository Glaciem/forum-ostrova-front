import React, { useEffect } from 'react';
// import * as Switch from '@radix-ui/react-switch';
import './styles.css';
import { observer } from "mobx-react-lite" // Or "mobx-react".
import {axiosInstance} from '../../api/Api';
import { useAppStore } from '../../store/AppStoreProvider';

import { Flex, Text, Switch, ScrollArea, Heading  } from '@radix-ui/themes';

const ShipChoises = () => {

  const appStore = useAppStore();

  useEffect(() => {
    axiosInstance.get("/ships")
    .then(function (response) {
      appStore.setShips(response.data);
      console.log(appStore.ships);
    })
    .catch(function (error) {
      console.log(error);
    })
    .finally(function () {
    });
  }, [])
    
    // return (<>
    //   {!appStore.ships ? "lol" : appStore.ships.map((ship) => (
    //     <div style={{ display: 'flex', alignItems: 'center' }}>
    //       <label className="Label" htmlFor="airplane-mode" style={{ paddingRight: 15 }}>
    //         {ship.name}
    //       </label>
    //       <Switch.Root className="SwitchRoot" id="airplane-mode">
    //         <Switch.Thumb className="SwitchThumb" />
    //       </Switch.Root>
    //     </div>
    //   ))}
    // </>)

    const setUsedShip = (shipId, check_status) => {
      if (check_status) {
        appStore.usedShips.push(shipId)
      }else{
        const index = appStore.usedShips.indexOf(shipId);
        if (index > -1) {
          appStore.usedShips.splice(index, 1);
        }
      }
      console.log(appStore.usedShips)
    } 

    return (<>

    <div style={{ height: 350, width: 300, border: "1px solid #aeaeae", borderRadius: "0.3rem" }}>
        <div style={{width: "100%", height: "50px", display: "flex", alignItems: "center", justifyContent: "center", backgroundColor: "#374150", borderBottom: "1px solid #aeaeae", borderRadius: "0.3rem"}} >
          <Heading> <font color="white">Доступные корабли</font> </Heading>
        </div>
        <ScrollArea type="auto" scrollbars="vertical" style={{ height: 298, width: "100%"}}>
          <Flex direction="column" gap="3" justify="start" align="start" style={{padding: "10px"}}>
            {!appStore.ships ? "ЗАГРУЗКА" : appStore.ships.map((ship) => (
                <Text size="2" key={ship.id}>
                  <label>
                    <Switch mr="2" onCheckedChange={(e) => {setUsedShip(ship.id, e)}} /> <Text>{ship.name}</Text>
                  </label>
                </Text>
            ))}
          </Flex>
        </ScrollArea>
    </div>
    </>)
};

export default observer(ShipChoises);