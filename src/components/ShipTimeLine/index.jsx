import { Tabs, Box, Flex, Card } from '@radix-ui/themes';
import { Button, Timeline } from 'flowbite-react';

const ShiptTimeline = (props) => {

    const {results} = props;

  return (
    <Tabs.Root defaultValue={results.lenght > 0 ? results[0].ship_name : null} >
        <Tabs.List>
            {results.map((ship, index) => (
                <Tabs.Trigger  value={ship.ship_name}>{ship.ship_name}</Tabs.Trigger>
            ))}
        </Tabs.List>
        <Box px="4" pt="3" pb="2">
            {results.map((ship) => (
                <Tabs.Content value={ship.ship_name}>
                    <Timeline>
                        {/* <Flex aligin={"start"} > */}
                        {Object.keys(ship.road).map((day) => (
                            <Timeline.Item>
                            <Timeline.Point />
                            <Timeline.Content>
                                <Flex align={"start"} direction={"column"}>
                            <Timeline.Time >
                                {new Date(day).toDateString()} {ship.road[day].time !== "*" ? ", " + ship.road[day].time + " Км/ч": null}
                            </Timeline.Time>
                            {/* <Timeline.Title>Средняя скорость {ship.road[day].time}</Timeline.Title> */}
                            <Flex align={"start"} direction={"row"}>
                            {Object.keys(ship.road[day].steps).map((step) => (
                                <Card style={{marginRight: "10px"}}>
                                <Timeline.Title>
                                    {ship.road[day].steps[step].start !== "-" && ship.road[day].steps[step].end !== "-" ? `Отправился из ${ship.road[day].steps[step].port_start} и прибыл в ${ship.road[day].steps[step].port_end}` : null }
                                    {ship.road[day].steps[step].start !== "-" && ship.road[day].steps[step].end === "-" ? `Отправился из ${ship.road[day].steps[step].port_start}` : null }
                                    {ship.road[day].steps[step].start === "-" && ship.road[day].steps[step].end !== "-" && ship.road[day].steps[step].end !== "on my way.." ? `Прибыл в ${ship.road[day].steps[step].port_end}` : null }
                                    {ship.road[day].steps[step].start === "-" && ship.road[day].steps[step].end === "-" && ship.road[day].time !== "*" ? "В пути" : null }
                                    {ship.road[day].steps[step].start === "-" && ship.road[day].steps[step].end === "on my way.." ? "В пути" : null }
                                    {ship.road[day].time === "*" ? "Стоял" : null }
                                </Timeline.Title>
                                <Timeline.Body>
                                <p>Тонны груза: {ship.road[day].steps[step].copacity}</p>                  
                                <p>Время старта: {ship.road[day].steps[step].start !== "-" ? `${ship.road[day].steps[step].start}:00` : "-"}</p>
                                <p>Время Прибытия: {ship.road[day].steps[step].end !== "-" && ship.road[day].steps[step].end !== "on my way.." ? `${ship.road[day].steps[step].end}:00` : "-"}</p>
                                {/* {ship.road[day].steps[step].start === "-" && ship.road[day].steps[step].end !== "-" && ship.road[day].steps[step].end !== "on my way.." ? `Разгрузка/Погрузка: ${ship.road[day].steps[step].full}:00` : "Разгрузка/Погрузка: -" } */}
                                </Timeline.Body>
                                </ Card>
                            ))}
                            </Flex>
                            </Flex>
                            </Timeline.Content>
                        </Timeline.Item>
                        ))}
                        {/* </Flex> */}
                    </Timeline>
                </Tabs.Content>
            ))}
        </Box>
    </Tabs.Root>
  )
}

export default ShiptTimeline;
