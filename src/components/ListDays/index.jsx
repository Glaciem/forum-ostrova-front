import React from 'react';
import { useState } from 'react';
// import * as Popover from '@radix-ui/react-popover';
// import { MixerHorizontalIcon } from '@radix-ui/react-icons';
import { Flex} from '@radix-ui/themes';
// import { useAppStore } from '../../store/AppStoreProvider';
import './styles.css';

import ScrollAreaDemo from '../ScrollArea';

const ListDays = () => {
    
  const [factors, setFactors] = useState([]);

  return (<>
    <Flex direction="column" gap="2">
      {/* <Box>
        <Text>Факторы</Text>
      </Box> */}
      <ScrollAreaDemo factors={factors} title={"События"} setFactors={setFactors}/>
    </Flex>

  </>)
}

export default ListDays;