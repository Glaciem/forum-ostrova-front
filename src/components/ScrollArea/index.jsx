import React, { useEffect, useState } from 'react';
// import * as ScrollArea from '@radix-ui/react-scroll-area';
import { MixerHorizontalIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import { DoubleArrowUpIcon, DoubleArrowDownIcon } from '@radix-ui/react-icons'
import { useAppStore } from '../../store/AppStoreProvider';
import { ScrollArea, Box, Text, Separator, Flex, Grid, Table, Heading} from '@radix-ui/themes';
import { Popover, TextField, Button, Select } from '@radix-ui/themes';
import './styles.css';;



export const PopoverDemo = (props) => {

  const appStore = useAppStore();

  const {setFactors} = props;
  
  const [factorType, setFactorType] = useState("up_speed");
  const [time, setTime] = useState("-2");
  const [date, setDate] = useState("2023-08-12");

  const handleClickAdd = () => {
    const newElem = {
      factorType: factorType,
      time: time,
      date: date
    }
    appStore.factors.push(newElem)
    setFactors(prev => {
      const newFactors = prev.concat([newElem]);
      return newFactors
    });
  }
  
  return (
  <Popover.Root>
  <Popover.Trigger>
    <button className="IconButton" aria-label="Update dimensions" style={{position: "absolute", bottom: "10px", right: "10px" }}>
      <PlusCircledIcon />
    </button>
  </Popover.Trigger>
  <Popover.Content style={{ width: 360 }} alignOffset={-100}>
    <Grid gap="3" rows="4" width="auto">
      <Select.Root
        defaultValue="up_speed"
        value={factorType}
        onValueChange={(value) => {
          setFactorType(value);
          if (value === "up_speed") {
            setTime("-2");
          } else if (value === "bad_weth"){
            setTime("+5");
          } else {
            setTime("+24");
          }
        }}>
        <Select.Trigger variant="surface" />
        <Select.Content>
          <Select.Item value="up_speed">Ускорение</Select.Item>
          <Select.Item value="bad_weth">Плохая погода</Select.Item>
          <Select.Item value="ice">Наледь</Select.Item>
          <Select.Item value="off_day">Нерабочий день</Select.Item>
        </Select.Content>
      </Select.Root>
        <TextField.Input placeholder="Часы" value={time} onChange={e => setTime(e.target.value)}/>
        <TextField.Input type='date' value={date} onChange={e => setDate(e.target.value)}/>
        <Popover.Close onClick={handleClickAdd}>
          <Button size="2">Добавить</Button>
        </Popover.Close>
    </Grid>
  </Popover.Content>
</Popover.Root>)
};

const ScrollAreaDays = (props) => {

  const {factors, title, setFactors} = props;
  // const appStore = useAppStore();

  return (<>
  <div style={{ height: 350, width: 300, border: "1px solid #aeaeae", borderRadius: "0.3rem" }}>
    <div style={{width: "100%", height: "50px", display: "flex", alignItems: "center", justifyContent: "center", backgroundColor: "#374150", borderBottom: "1px solid #aeaeae", borderRadius: "0.3rem"}} >
      <Heading> <font color="white">{title}</font> </Heading>
    </div>
  <ScrollArea type="auto" scrollbars="vertical" style={{ height: 298, width: "100%"}}>
  <Table.Root>
    {/* <Table.Header>
      <Table.Row>
        <Table.ColumnHeaderCell ></Table.ColumnHeaderCell>
        <Table.ColumnHeaderCell >{title}</Table.ColumnHeaderCell>
        <Table.ColumnHeaderCell ></Table.ColumnHeaderCell>
      </Table.Row>
    </Table.Header> */}
    <Table.Body>
    {factors.length !== 0 ? factors.map((day, index) => (
            <Table.Row>
              <Table.Cell>
                <Flex direction="row" align={"center"}>
                {day.factorType === "up_speed" ? <DoubleArrowUpIcon size="3" radius="full" fallback="T" /> :
                <DoubleArrowDownIcon size="3" radius="full" fallback="T" /> 
                }
                <Separator orientation="vertical" style={{marginLeft: "2px", marginRight: "2px"}}/>
                  <Text as="div" size="2" weight="bold">
                    {day.factorType === "up_speed" ? "Ускорение" : null}
                    {day.factorType === "bad_weth" ? "Плохая погода" : null}
                    {day.factorType === "ice" ? "Наледь" : null}
                    {day.factorType === "off_day" ? "Нерабочий день" : null}
                  </Text>
                </Flex>
              </Table.Cell>

              <Table.Cell>
                <Flex align={"center"}>
                  <Text as="div" size="2" color="gray" align={"center"}>
                    {new Date(day.date).toLocaleDateString("ru-RU")}
                  </Text>
                </Flex>
              </Table.Cell>

              <Table.Cell>
                <Flex align={"center"}>
                  {day.time.slice(0,1) === "+" ? <Text as="div" size="5" color="red" weight="bold" align={"center"}>{day.time}</Text> : null}
                  {day.time.slice(0,1) === "-" ? <Text as="div" size="5" color="green" weight="bold" align={"center"}>{day.time}</Text> : null}
                  {day.time.slice(0,1) !== "+" && day.time.slice(0,1) !== "-" ? <Text as="div" size="5" color="gray" weight="bold" align={"center"}>{day.time}</Text> : null}
                </Flex>
              </Table.Cell>
            </Table.Row>
    ))
    : <Table.Row><Table.Cell style={{textAlign: "center"}}><Text>Событий нет</Text></Table.Cell></Table.Row>
  }

      </Table.Body>
    </Table.Root>
    <PopoverDemo setFactors={setFactors}/>
  </ScrollArea>

  
  </div>
  </>)
};

export default ScrollAreaDays;