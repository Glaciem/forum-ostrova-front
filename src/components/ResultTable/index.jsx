import { Flex, Heading, Table, Text } from '@radix-ui/themes';


const ResultTable = (props) => {

    const {results} = props;

    return (
        <Table.Root variant="surface">
              <Table.Header >
                <Table.Row style={{backgroundColor: "#374150"}}>
                    <Table.ColumnHeaderCell style={{color: "white"}}>Судно</Table.ColumnHeaderCell>
                  <Table.ColumnHeaderCell style={{color: "white"}}>Рейсы</Table.ColumnHeaderCell>
                  <Table.ColumnHeaderCell style={{color: "white"}}>Легковые авто</Table.ColumnHeaderCell>
                  <Table.ColumnHeaderCell style={{color: "white"}}>Грузовые авто</Table.ColumnHeaderCell>
                  <Table.ColumnHeaderCell style={{color: "white"}}>Вагоны</Table.ColumnHeaderCell>
                  <Table.ColumnHeaderCell style={{color: "white"}}>Тонн на судно</Table.ColumnHeaderCell>
                  <Table.ColumnHeaderCell style={{color: "white"}}>Всего тонн</Table.ColumnHeaderCell>
                </Table.Row>
              </Table.Header> 
              <Table.Body>
                {results.length !== 0  ? results.map((result, index) => (
                    <Table.Row>
                        <Table.Cell>{result.ship_name}</Table.Cell>
                        <Table.Cell>{result.total.steps}</Table.Cell>
                        <Table.Cell>{result.total.auto_light}</Table.Cell>
                        <Table.Cell>{result.total.auto_big}</Table.Cell>
                        <Table.Cell>{result.total.train}</Table.Cell>
                        <Table.Cell>{result.total.total}</Table.Cell>
                        {index === 0 ?
                            <Table.Cell rowSpan={results.length}>
                                <Flex align={"center"} justify={"center"} style={{height: "100%"}}>
                                    <Heading size="3">
                                        {results.reduce((accumulator, currentValue) => accumulator + currentValue.total.total, 0)}
                                    </Heading>
                                </Flex>
                            </Table.Cell> :
                         null}
                    </Table.Row>
                )) :
                <Table.Row>
                    <Table.Cell style={{textAlign: "center"}}>Данные еще не обработаны</Table.Cell>
                    {/* <Text style={{textAlign: "center"}}>Данные еще не обработаны</Text> */}
                </Table.Row>
            
            }
                
              </Table.Body>
            </Table.Root>
    )
}

export default ResultTable;