import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useState, useEffect } from "react";
import { useAppStore } from '../../store/AppStoreProvider';
import "./styles.css"

const MyDatePicker = () => {

    const appStore = useAppStore();

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    const onChange = (dates) => {
      const [start, end] = dates;
      setStartDate(start);
      setEndDate(end);
    };

    useEffect(() => {
        appStore.period.start = startDate;
        appStore.period.end = endDate;
    }, [startDate, endDate])

    return (
        <div className="customDatePickerWidth">
            <DatePicker
                selected={startDate}
                onChange={onChange}
                minDate={new Date()}
                // maxDate={addMonths(new Date(), 5)}
                startDate={startDate}
                endDate={endDate}
                selectsRange
                inline
                showDisabledMonthNavigation
            />
        </div>
    );
  };

export default MyDatePicker;